function notes#mksession(file)
    let save_sessionsoptions = &sessionoptions
    let &sessionoptions = 'curdir,tabpages,winsize'
    exec 'mksession! ' . a:file
    let &sessionoptions = save_sessionsoptions
    unlet save_sessionsoptions
endfunction
