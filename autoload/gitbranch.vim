scriptencoding utf-8

if ! exists('g:loaded_fugitive')
    function! gitbranch#GitInfo()
        return ''
    endfunction
else
    function! gitbranch#GitInfo()
      let l:git = fugitive#head(8)
      if l:git !=? ''
        return '(' . l:git . ')'
      else
        return ''
        endif
    endfunction
endif
