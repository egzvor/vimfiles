function! statusline#stub(func_name)
    if exists('*' . a:func_name)
        return funcref(a:func_name)
    else
        return { -> ''}
    endif
endfunction

function! statusline#eol()
    return &endofline ? '' : '[noeol]'
endfunction

" path_shorten returns the shortened version of the path {{{
" suitable to use in a statusline.
" It will not shorten the last element even if it is a directory.
" It will not shorten anything if the path is already short.
" ----------------------------------------------------------------------
" Examples:
"     /home/user/dotvim.git/current/vimrc ->
"     ~/dotvim.git/current/vimrc
" ----------------------------------------------------------------------
"     ./pack/minpac/start/vim-fugitive/autoload/fugitive.vim ->
"     pack/minp/star/vim-/auto/fugitive.vim
" ----------------------------------------------------------------------
"     /home/user/repos/vim/src/libvterm/src/encoding/uk.tbl ->
"     ~/repo/vim/src/libv/src/enco/uk.tbl
" ----------------------------------------------------------------------
" \param path
" \type string
"     Path to shorten.
" \param elem_length
" \type integer
"     The maximum number of on-screen characters for each path
"     component.
" \param total_length
" \type integer
"     The maximum number of on-screen characters for the whole path.
"     Returned string may be longer if the path is very deep. We shorten
"     as much as we can.
" \return
" \type string
"     Shortened path. }}}
function! statusline#path_shorten(path, elem_length, total_length)
    let path = substitute(fnamemodify(a:path, ':~:.'), $HOME, '~', '')
    if strdisplaywidth(path) < a:total_length
        return path
    endif

    let tail = fnamemodify(path, ':t')
    let head = fnamemodify(path, ':~:.:h')
    " pathshorten shortens tailing directory if it has a trailing slash,
    " so we only add a slash when path ends with a file.
    if tail !=# ''
        let head = head . '/'
    endif
    return pathshorten(head, a:elem_length) . tail
endfunction

function! statusline#nc() abort
    return g:actual_curwin != win_getid()
endfunction

function! statusline#colorpath() abort
    let hlGroup = MarksMapsGet('color')
    let path_item = '[%n] %{statusline#path_shorten(expand("%f"), 4, 40)}'
    if hlGroup ==? ''
        return path_item
    endif

    return    '%#MarkColorPrefix' .. hlGroup .. '#🭄%*'
\          .. '%{% "%#MarkColor" .. (statusline#nc() ? "NC": "") .. "' .. hlGroup .. '#" %}'
\          .. path_item
\          .. '%#MarkColorPrefix' .. hlGroup .. '#🭡%*'
endfunction

function! statusline#ale() abort
    if !exists('g:loaded_ale')
        return ''
    endif

    try
        return ale#statusline#Count(bufnr())['total'] > 0 ? '[!]' : ''
    catch /Unknown function.*ale#statusline#Count/
        return ''
    endtry
endfunction

function! statusline#statusline() abort
    let status_lines =<< trim END
        %{%statusline#colorpath()%}
        %#StatusLineNC#
         %h%m%r%w
        %{statusline#eol()}
        %{%statusline#ale()%}
        %=
        %{winnr() == winnr('$') ? '󰷏 ' .. v:servername : ''}
         [%1l/%1L]:%1v
         %y
        %{statusline#stub("ObsessionStatus")()}
    END
    return join(status_lines, '')
endfunction
