if exists('g:loaded_qftoggle')
    finish
endif

let g:loaded_qftoggle = 1

function! s:GetQFWinID()
    for win_info in getwininfo()
        if win_info['quickfix'] && !win_info['loclist']
            return win_info['winid']
        endif
    endfor
    return 0
endfunction

function! ToggleQuickfix(follow)
    let l:last_window_id = win_getid()
    let l:qf_win_id = s:GetQFWinID()
    if l:qf_win_id == 0
        botright copen
        if !a:follow
            wincmd p
        endif
    else
        if l:qf_win_id == l:last_window_id
            wincmd p
        else
            call win_gotoid(l:last_window_id)
        endif
        cclose
    endif
endfunction

function! ToggleLocationFix()
    let nr = winnr('$')
    belowright lwindow
    let nr2 = winnr('$')
    if nr == nr2
        lclose
    endif
endfunction

command! -bar -bang -nargs=0 ToggleQuickfix call ToggleQuickfix("<bang>" == "!" ? 1 : 0)
command! -bar -nargs=0 ToggleLocationFix call ToggleLocationFix()

augroup AutoQuickfixToggle
    autocmd!
    " QuickFixCmdPost commands are overwritten at:
    "   after/plugin/config/quickfix-reflector.vim
    autocmd QuickFixCmdPost cgetexpr,vimgrep,vimgrepadd copen
    autocmd QuickFixCmdPost lgetexpr,lvimgrep,lvimgrepadd lopen
    " automatically close corresponding loclist when quitting a window
    if exists('##QuitPre')
        autocmd QuitPre * nested if &filetype != 'qf' | silent! lclose | endif
    endif
augroup END
