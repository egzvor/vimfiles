" Note: we have to load packages immediately (packadd without !), because
" `packloadall` is already called at the time FileType event is fired.

if !exists('g:loaded_minpac')
    finish
endif

let s:filetype_to_plugins = {}

function s:load_package_config(p)
    let config = get(get(g:minpac#pluglist, a:p, {}), 'config', '')
    if config !=# ''
        exe 'runtime ' . config
    endif
endfunction

function s:add_optional_packages(packages)
    for p in a:packages
        exe 'packadd ' . p
    endfor
    " Load configs after all packages in case there are inter-dependencies.
    " E.g., asyncomplete-buffer.vim depends on asyncomplete.vim
    for p in a:packages
        call s:load_package_config(p)
    endfor
endfunction

for [p, options] in items(g:minpac#pluglist)
    let filetypes = get(options, 'for', [])
    for ft in filetypes
        " Associate a package with a filetype.
        " Dict is used as a rudimentary hash map.
        if has_key(s:filetype_to_plugins, ft)
            let s:filetype_to_plugins[ft][p] = 0
        else
            let s:filetype_to_plugins[ft] = {p: 0}
        endif
    endfor
endfor

augroup MinpacFor
    au!
    exe 'autocmd FileType '
\       .. join(keys(s:filetype_to_plugins), ',')
\       .. ' call s:add_optional_packages('
\       .. 's:filetype_to_plugins->get(expand("<amatch>"), {})->keys()'
\       .. ')'
augroup END

command! -nargs=? -complete=packadd LoadPackages call s:add_optional_packages(split(<q-args>))
