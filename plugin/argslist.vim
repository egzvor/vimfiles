if exists('g:loaded_argslist')
    finish
endif

let g:loaded_argslist = 1

function! ListArgs()
    let args_list = argv()
    let counter = 1
    for arg in args_list
        if counter == argidx() + 1
            echo counter . ' ' . arg . ' <---'
        else
            echo counter . ' ' . arg
        endif
        echo ""
        let counter = counter + 1
    endfor
endfunction
