function! s:disable_cursor() abort
    if &t_ve !=? ''
        set t_ve=
    endif
endfunction

function! s:enable_cursor() abort
    if &t_ve ==# ''
        set t_ve&
    endif
endfunction

function! s:toggle_cursor() abort
    if &t_ve ==# ''
        set t_ve&
    else
        set t_ve=
    endif
endfunction

augroup CursorHide
    au!
    autocmd VimLeave,ModeChanged,FocusGained * CursorShow
    " autocmd VimLeave,ModeChanged,FocusGained,CursorMoved * CursorShow
    " autocmd CursorHold * CursorHide
augroup END

command! -nargs=0 CursorShow call s:enable_cursor()
command! -nargs=0 CursorHide call s:disable_cursor()
command! -nargs=0 CursorToggle call s:toggle_cursor()

nnoremap g<bs> <cmd>CursorToggle<cr>
nnoremap <middlemouse> <cmd>CursorToggle<cr>
