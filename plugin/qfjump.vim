" QuickfixNearest sets location list current entry to the location nearest to
" the cursor. If [!] is provided it also focuses the entry in the list.
function s:QuickfixNearest(focus_list)
  let errors = getqflist()
  let current_ln = getcurpos()[1]
  let target_pos = []

  let min_diff = 999
  let min_idx = 1
  for [id, entry] in mapnew(errors, {id, value -> [id + 1, value]})
      let diff = abs(entry['lnum'] - current_ln)
      if diff < min_diff
          let min_diff = diff
          let min_idx = id
      endif
  endfor

  if a:focus_list
      execute ':' . min_idx . 'cc'
  else
      call setqflist([], 'a', #{idx: min_idx})
  endif
endfunction

" LocListNearest sets location list current entry to the location nearest to the
" cursor. If [!] is provided it also focuses the entry in the list.
function s:LocListNearest(focus_list)
  let errors = getloclist(0)
  let current_ln = getcurpos()[1]
  let target_pos = []

  let min_diff = 999
  let min_idx = 1
  for [id, entry] in mapnew(errors, {id, value -> [id + 1, value]})
      let diff = abs(entry['lnum'] - current_ln)
      if diff < min_diff
          let min_diff = diff
          let min_idx = id
      endif
  endfor

  if a:focus_list
      execute ':' . min_idx . 'll'
  else
      call setloclist(0, [], 'a', #{idx: min_idx})
  endif
endfunction

command -bang LNearest call s:LocListNearest("<bang>" == "!")
command -bang CNearest call s:QuickfixNearest("<bang>" == "!")
