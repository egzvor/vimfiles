function! s:duplicate_tab() abort
    let s:sessionoptions = &sessionoptions
    try
        let &sessionoptions = 'blank,help,folds,winsize,localoptions'
        let s:file = tempname()
        execute 'mksession ' . s:file
        tabnew
        execute 'source ' . s:file
    finally
        silent call delete(s:file)
        let &sessionoptions = s:sessionoptions
        unlet! s:file s:sessionoptions
    endtry
endfunction

command! DuplicateTab call s:duplicate_tab()

nnoremap <c-w><c-t> <cmd>DuplicateTab<cr>
