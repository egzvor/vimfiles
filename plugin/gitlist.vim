if exists('g:loaded_gitlist')
    finish
endif

let g:loaded_gitlist = 1

" Letter to word mapping for git diff output
let s:git_status_dictionary = {
\   'A': 'Added',
\   'B': 'Broken',
\   'C': 'Copied',
\   'D': 'Deleted',
\   'M': 'Modified',
\   'R': 'Renamed',
\   'T': 'Changed',
\   'U': 'Unmerged',
\   'X': 'Unknown',
\}

" Return the list of files changed compared to <rev>.
function! ListCommitFiles(...) abort
    if a:0 == 0
        let rev = 'HEAD'
    else
        let rev = a:1
    endif
    let file_dir = shellescape(expand('%:p:h'))
    let git_root = substitute(system('cd ' . file_dir . '; git rev-parse --show-toplevel'), '\n', '', 'g')
    let status_lines = systemlist('cd ' . file_dir . '; git diff --name-status ' . rev . '~ ' . rev)
    let insertion_number_lines = systemlist('cd ' . file_dir . '; git diff --numstat ' . rev . '~ ' . rev)
    let file_list = []
    for idx in range(len(status_lines))
        let status = status_lines[idx]
        let insertions = insertion_number_lines[idx]
        call add(file_list, #{
\           filename: git_root . '/' . matchstr(status, '\v\S+$'),
\           text: printf(
\               '%s (+%d -%d)',
\               get(s:git_status_dictionary, matchstr(status, '\v^\w'), 'Unknown'),
\               matchstr(insertions, '\v^\d+'),
\               matchstr(insertions, '\v^\d+\s+\zs\d+')
\           ),
\           lnum: 1,
\           col: 1
\        })
    endfor
    return file_list
endfunction

function! ShowAllDiffs(ref)
    silent tabonly | silent wincmd o
    exec 'silent cfdo silent! aboveleft Gvdiffsplit ' .. a:ref .. ' | tab split'
    tabclose
    1 tabnext
endfunction

command! -nargs=? -bar GlistDiff call setqflist([], ' ', {'nr': '$', 'title': 'GlistDiff <args>', 'items': ListCommitFiles(<f-args>)}) | cwindow
command! -nargs=? GAllDiffs call ShowAllDiffs('<args>' ?? 'HEAD~')
command! Review call ShowAllDiffs(g:diff_base)
