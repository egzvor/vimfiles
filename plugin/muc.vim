if exists('g:loaded_matchundercursor')
    finish
endif

let g:loaded_matchundercursor = 1

hi default link UnderCursorMatch Search

function! ToggleUnderCursorMatch()
    let group = 'UnderCursorMatch'
    let matches = getmatches()
    let matches = filter(matches, {idx, val -> val['group'] ==# group})
    if len(matches) > 0
        for match in matches
            call matchdelete(match['id'])
        endfor
    else
        let pattern = '\<' . expand('<cword>') .'\>'
        let s:id =  matchadd(group, pattern)
    endif
endfunction
