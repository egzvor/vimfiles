" Analyze existing logs first 16-05-24
finish

augroup VimSensei
    au!
    autocmd VimEnter     * call ch_logfile($HOME .. '/.vim/log-' .. strftime('%s'))
    autocmd InsertEnter  * call ch_log("::Entering Insert Mode::")
    autocmd InsertLeave  * call ch_log("::Leaving Insert Mode::")
    autocmd CmdwinEnter  * call ch_log("::Entering command-line window::")
    autocmd CmdwinLeave  * call ch_log("::Leaving command-line window::")
    autocmd CmdlineEnter * call ch_log("::Entering command-line mode::")
    autocmd CmdlineLeave * call ch_log("::Leaving command-line mode::")
augroup END
