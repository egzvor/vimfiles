vim9script

augroup Servername
    au!
    autocmd VimEnter * {
        if v:servername != ''
            silent! call system(
                $'{$HOME}/scripts/vim/save-pid-for-server.py {v:servername} {getpid()}'
            )
        endif
    }
augroup END
