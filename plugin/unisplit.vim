if exists('g:unisplit_loaded')
    finish
endif

let g:unisplit_loaded = 1

function! UniversalSplit(kind)
    let l:command = getcmdline()
    let l:keymap = #{
\       horizontal: #{
\           buffer: "\<home>s\<end>\<cr>",
\           edit: "\<home>\<s-right>\<c-w>split \<cr>",
\           literal: "",
\       },
\       vertical: #{
\           buffer: "\<home>vert s\<end>\<cr>",
\           edit: "\<home>\<s-right>\<c-w>vsplit \<cr>",
\           literal: "",
\       },
\       tab: #{
\           buffer: "\<home>tab s\<end>\<cr>",
\           edit: "\<home>\<s-right>\<c-w>tab split \<cr>",
\           literal: "",
\       },
\    }

    if l:command =~# '^buffer'
        return l:keymap[a:kind]['buffer']
    elseif l:command =~# '^edit'
        return l:keymap[a:kind]['edit']
    else
        return l:keymap[a:kind]['literal']
    endif
endfunction
