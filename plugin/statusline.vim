let s:vimproviser_popup_id = v:null

function! s:vimproviser_popup() abort
    call popup_close(s:vimproviser_popup_id)
    let s:vimproviser_popup_id = popup_create(
\       VimproviserStatus() == 'Characters' ? '' : VimproviserStatus(),
\       #{
\           highlight: 'VimproviserStatus',
\           pos: 'botright',
\           line: &lines-2,
\           col: &columns,
\           tabpage: -1,
\           zindex: 300,
\       }
\   )
endfunction

augroup Statusline
    au!
    autocmd User VimproviserSet call s:vimproviser_popup()
augroup END
