vim9script

const DURATION = 300

def HighlightedYank()
    if v:event.operator !=? 'y'
        return
    endif

    var [yank_start, yank_end] = [getpos("'["), getpos("']")]
    var regtype = v:event.regtype ?? 'v'
    var positions = getregionpos(yank_start, yank_end, {type: regtype})
    var end_offset = (regtype == 'V' || v:event.inclusive) ? 1 : 0
    var fixed_positions = mapnew(positions, (_, pos) => {
        var [start, end] = pos
        end->SetColumn(end->Column() + end_offset)
        return [start, end]
    })
    var match_id = MatchAddPos(HighlightGroup(v:event), fixed_positions)
    var winid = win_getid()
    timer_start(DURATION, (_) => match_id->matchdelete(winid))
enddef

def MatchAddPos(hlgroup: string, positions: list<list<any>>): number
    return matchaddpos(hlgroup, positions->mapnew((_, pos) => {
            var [start, end] = pos
            return [
                start->LineNum(),
                start->Column(),
                end->Column() - start->Column()
            ]
        }))
enddef

def HighlightGroup(event: dict<any>): string
    return event.regname ==? '+' ? 'HighlightedyankRegionClipboard' : 'HighlightedyankRegion'
enddef

def LineNum(pos: list<number>): number
    return pos[1]
enddef

def SetColumn(pos: list<number>, value: number)
    pos[2] = value
enddef

def Column(pos: list<number>): number
    return pos[2]
enddef

augroup highlightedyank
    autocmd!
    autocmd TextYankPost * HighlightedYank()
augroup END
