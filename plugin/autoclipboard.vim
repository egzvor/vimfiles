const s:TIMEOUT = 3000

let s:copy_to_clipboard = v:false
let s:timer_id = v:null

function! s:reset(_) abort
    let s:copy_to_clipboard = v:false
endfunction

function! s:set() abort
    if s:timer_id != v:null
        call timer_stop(s:timer_id)
    endif
    let s:copy_to_clipboard = v:true
    let s:timer_id = timer_start(s:TIMEOUT, funcref('s:reset'))
endfunction


augroup Autoclipboard
    au!
    autocmd TextYankPost         * if v:event.regname ==# '' | call s:set() | endif
    autocmd FocusLost,VimSuspend * if s:copy_to_clipboard    | let @+=@@    | endif
    " Pass ownership of the clipboard to an external program
    " so that it's not lost.
    autocmd VimLeave,VimSuspend  * call system("xsel -ib", getreg('+'))
augroup END
