if exists('g:loaded_marks_maps')
    finish
endif

let g:loaded_marks_maps = 1

if !exists('g:marks_maps')
    let g:marks_maps = {}
endif

function! MarksMapsGet(group) abort
    let mapping = get(g:marks_maps, a:group, {})
    if len(mapping) == 0
        return ''
    endif

    let global_marks_info = getmarklist()
    for mark_info in global_marks_info
        if fnamemodify(mark_info['file'], ':p') !=# fnamemodify(bufname(), ':p')
            continue
        endif
        for [mark, value] in items(mapping)
            if mark_info['mark'][1] ==# mark
                return value
            endif
        endfor
    endfor

    return ''
endfunction
