" fFTt for any upper-case character

" TODO: count? repeat?
function! s:zaglav(f) abort
    " if a:count == 0
    "     let local_count = a:count
    " else
    "     let local_count = a:count - 1
    " endif

    let pat = '[A-Z]'
    let flags = ''
    " if a:f ==# 'f'
    "     let flags = flags .. 'e'
    if a:f ==# 'T'
        let pat = pat .. '.'
        let flags = flags .. 'e'
    elseif a:f ==# 't'
        let pat = '.' .. pat
    endif

    if a:f =~# '[FT]'
        let flags = flags .. 'b'
    endif

    " let pat_with_count = pat .. ('.\{-}' .. pat)->repeat(local_count)
    call search(pat, flags, line('.'))
endfunction

nnoremap f<cr> <cmd>call <sid>zaglav('f')<cr>
xnoremap f<cr> <cmd>call <sid>zaglav('f')<cr>
nnoremap t<cr> <cmd>call <sid>zaglav('t')<cr>
xnoremap t<cr> <cmd>call <sid>zaglav('t')<cr>
nnoremap F<cr> <cmd>call <sid>zaglav('F')<cr>
xnoremap F<cr> <cmd>call <sid>zaglav('F')<cr>
nnoremap T<cr> <cmd>call <sid>zaglav('T')<cr>
xnoremap T<cr> <cmd>call <sid>zaglav('T')<cr>

function! s:operator_zaglav(f) abort
    exe 'normal v' .. a:f .. "\<cr>"
endfunction
onoremap f<cr> <cmd>call <sid>operator_zaglav('f')<cr>
onoremap F<cr> <cmd>call <sid>operator_zaglav('F')<cr>
onoremap t<cr> <cmd>call <sid>operator_zaglav('t')<cr>
onoremap T<cr> <cmd>call <sid>operator_zaglav('T')<cr>
