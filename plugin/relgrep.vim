if exists('g:loaded_relgrep')
    finish
endif

let g:loaded_relgrep = 1

function! s:with_grep_format(cmd, prg, args, path) abort
    let l:args = escape(a:args, '\"')
    let l:saved_errorformat = &errorformat
    let &errorformat = &grepformat
    try
        if a:path ==# ''
            exe a:cmd . ' system("' . a:prg . ' ' . l:args . ' ")'
        else
            exe a:cmd . ' system("' . a:prg . ' ' . l:args . ' ' . shellescape(a:path) . '")'
        endif
    finally
        let &errorformat = l:saved_errorformat
    endtry
endfunction

command!  -complete=file_in_path -nargs=+ GrepProject  call s:with_grep_format('cgetexpr', &grepprg,    <q-args>, ''            )
command!                         -nargs=+ GrepDir      call s:with_grep_format('cgetexpr', &grepprg,    <q-args>, expand('%:p:h'))
command!  -complete=file_in_path -nargs=+ LGrepProject call s:with_grep_format('lgetexpr', &grepprg,    <q-args>, ''            )
command!                         -nargs=+ LGrepDir     call s:with_grep_format('lgetexpr', &grepprg,    <q-args>, expand('%:p:h'))
command!                         -nargs=+ LGrepFile    call s:with_grep_format('lgetexpr', &grepprg,    <q-args>, expand('%:p') )
command!                         -nargs=+ GrepDiff     call s:with_grep_format('cgetexpr', 'grep_diff', <q-args>, ''            )
