unlet! skip_defaults_vim
runtime defaults.vim

" Options {{{
set autoindent
set clipboard-=autoselect clipboard^=autoselectml
set belloff=all
set complete-=i
set completeopt+=popup
set completepopup+=highlight:LineNr,border:off
set conceallevel=0
set confirm
set diffopt=filler,hiddenoff,algorithm:histogram,context:12,foldcolumn:0
set encoding=utf-8
scriptencoding utf-8
set errorformat+=%f\\\|%l\ col\ %c\\\|\ %m
set expandtab softtabstop=4 tabstop=8 shiftwidth=4
set fillchars=vert:🭲,fold:┈,diff:┈,eob:\ 
set foldcolumn=0 foldlevelstart=99
set hidden
set laststatus=2
set infercase ignorecase smartcase
set list listchars=tab:··
set pastetoggle=<F2>
set nostartofline
set termguicolors
set signcolumn=no
set shortmess+=I
set noshowcmd
set scrollfocus
set scrolloff=3
set splitbelow splitright
set tags=./tags;$HOME,tags
set ttyfast
set undofile undodir=$HOME/.vim/undodir
" Don't store too much search and input history
set viminfo+=/100,@100
" Search upwards for a manually created .viminfo file or use a default
let &viminfofile=fnamemodify(
\   findfile('.viminfo', './;' .. $HOME) ??  $HOME .. '/.vim/viminfo',
\   ':p',
\)
set wildcharm=<c-z>
set wildignorecase
if has('patch-8.2.4325')
    set wildmode=full
    set wildoptions+=tagfile,pum
elseif has('patch-8.1.2225')
    set wildmode=list:lastused,full
else
    set wildmode=list:full,full
endif

set keymap=russian-from-hd-gold iminsert=0 imsearch=-1 spelllang=en_gb,ru_ru

if executable('rg')
    let &grepprg = 'rg --vimgrep --no-heading --smart-case --follow'
elseif executable('grep')
    let &grepprg = 'grep -nR --ignore-case --exclude-dir=".env" --exclude="tags" --exclude="*.swp" --exclude="*.swo" --exclude="*.pyc"'
endif
set grepformat=%f:%l:%c:%m,%f:%l:%m

if &shell =~# 'fish$'
    set shell=zsh
endif

" Cursor shape
if &term =~? 'rxvt' || &term =~? 'xterm' || &term =~? 'st-'
    " 1 or 0 -> blinking block
    " 2 -> solid block
    " 3 -> blinking underscore
    " 4 -> solid underscore
    " Recent versions of xterm (282 or above) also support
    " 5 -> blinking vertical bar
    " 6 -> solid vertical bar
    " Insert Mode
    let &t_SI .= "\<Esc>[6 q"
    " Normal Mode
    let &t_EI .= "\<Esc>[2 q"
endif

let &statusline = statusline#statusline()
" }}}

" Commands {{{
command! PlugUpdate call minpac#update()
command! PlugClean call minpac#clean()
command! PlugStatus call minpac#status()

command! -nargs=* Terminal <mods> split | lcd %:h | terminal ++curwin <args>
command! -range=% Ybuffer <line1>,<line2>y+
command! Ypath let @+=expand('%')
command! Server echo "Server: " .. v:servername

command! LongLinesSearch silent lvimgrep /\%>79v.\+/ %
command! LongLinesHighlight match ErrorMsg /\%>79v.\+/

" Quickfix/Location list management
command! QuickfixToLocList call setloclist(0, getqflist())
command! LocListToQuickfix call setqflist(getloclist(0))
command! QuickfixAddCurrent caddexpr expand('%') .. ':' .. line('.') ..  ':' .. getline('.')
command! LocListAddCurrent laddexpr expand('%') .. ':' .. line('.') ..  ':' .. getline('.')
command! LocListClear call setloclist('.', [])
command! TagStackToQuickfix call tagstack#setqf()
command! ArgsToQuickfix call setqflist([], ' ', {
\    'items': argv()->map({id, f -> {'filename': f, 'lnum': 1, 'text': id + 1}}),
\    'title': 'Argument list',
\})

" Run Vifm bound to the current Vim server
function! Vifm() abort
    if &term ==? 'xterm-kitty'
        if v:servername ==# ''
            echo 'Run vim with --servername first!'
        else
            execute 'silent !kitty --detach vifm --server-name ' .. v:servername .. ' ' .. getcwd()
        endif
        redraw!
    else
        echoerr 'Vifm is not implemented for ' .. &term
    endif
endfunction

command! Vifm call Vifm()

function! TabOutput(cmd)
    redir => message
    silent execute a:cmd
    redir END

    if empty(message)
        echoerr "no output"
        return
    endif

    " use "new" instead of "tabnew" below if you prefer split windows instead of tabs
    tabnew
    setlocal buftype=nofile bufhidden=wipe noswapfile nobuflisted nomodified
    silent put=message
endfunction
command! -nargs=+ -complete=command TabOutput call TabOutput(<q-args>)

command! -range SqlFormat <line1>,<line2>!sqlformat --keywords upper --identifiers lower --reindent --indent_width 4 --indent_columns --use_space_around_operators -
" }}}

" Mappings {{{
let mapleader="\<space>"
let maplocalleader = ','

nnoremap <space> <nop>
nnoremap \ ,
xnoremap \ ,
onoremap \ ,

" Modifying default key bindings {{{

nnoremap Y y$

" Column-precise movements with ` are more useful
noremap ` '
noremap ' `
noremap '' ``
noremap `` ''
sunmap '
sunmap `
sunmap ''
sunmap ``
nnoremap ]' ]`
nnoremap [' [`
nnoremap ]` ]'
nnoremap [` ['

" Break insert change when making a new line.
" That way each inserted line is a different change that you can undo, without
" loosing all the lines typed before the last one.
inoremap <cr> <c-]><c-g>u<cr>

" Go to previous buffer, but ignore unloaded ones
nnoremap <silent> <bs> <cmd>exe v:count ? v:count .. 'b' : 'b' .. (bufloaded(0) ? '#' : 'n')<cr>

" Don't include leading whitespace
onoremap a' 2i'
onoremap a" 2i"
xnoremap a' 2i'
xnoremap a" 2i"
onoremap gn gn<cmd>set hlsearch<cr>

nnoremap ) <cmd>call search('[^[:alnum:][:space:]]', 'e')<cr>
nnoremap ( <cmd>call search('[^[:alnum:][:space:]]', 'eb')<cr>
onoremap ) <cmd>call search('[^[:alnum:][:space:]]', 'e')<cr>
onoremap ( <cmd>call search('[^[:alnum:][:space:]]', 'eb')<cr>

nnoremap h zz
nnoremap j zt
nnoremap k zb

nnoremap z<down> zj
nnoremap z<up> zk

" }}}

" Adding new behaviour {{{

" Choose completion candidate with Tab.
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Marks {{{

nnoremap <leader>r 'R
nnoremap <leader>s 'S
nnoremap <leader>n 'N
nnoremap <leader>c 'D
nnoremap <leader>o 'B

let s:letters = 'abcdefghijklmnopqrstuvwxyz'

" Capital marks are more useful.
for ch in s:letters
    exe 'nnoremap m' .. ch ..          ' m' .. toupper(ch)
    exe 'nnoremap m' .. toupper(ch) .. ' m' .. ch
    exe "nnoremap '" .. ch ..          ' `' .. toupper(ch)
    exe "nnoremap '" .. toupper(ch) .. ' `' .. ch
endfor

function! s:lower_case_marks() abort
    return getmarklist('%')
\       ->filter({_, m -> match(m.mark[1], '[a-z]') >= 0})
endfunction

function! ToggleLocalMark() abort
    " Leave the first 5 letters for manual usage.
    let abc = s:letters[5:]->split('\zs')
    let lower_case_marks = s:lower_case_marks()

    let current_marks = lower_case_marks->copy()->filter({_, m -> m.pos[1] == line('.')})
    if len(current_marks) > 0
        for curmark in current_marks
            call setpos(curmark.mark, [0, 0, 0, 0])
        endfor
        return
    endif

    let available_idx = abc->indexof({_, alpha ->
\       lower_case_marks->indexof({_, lower -> lower.mark[1] == alpha}) < 0
\   })
    if available_idx < 0
        echoerr 'No available marks left'
        return
    endif

    call setpos("'" .. abc[available_idx], getcurpos())
endfunction
nnoremap m, <cmd>call ToggleLocalMark()<cr>

function! MarksToLocList() abort
    let marks = s:lower_case_marks()
    if len(marks) == 0
        return
    endif

    call setloclist(0,
\       marks
\       ->map({_, m -> {
\           'bufnr': bufnr(),
\           'lnum': m.pos[1],
\           'col': m.pos[2],
\           'text': printf('[%s] %s', m.mark[1], getline(m.pos[1]))
\       }})
\       ->sort({e1, e2 -> e1['lnum'] - e2['lnum']})
\   )
endfunction
nnoremap m\ <cmd>call MarksToLocList() \| lopen<cr>

function! MarkWithColor() abort
    if MarksMapsGet('color') !=? ''
        " Already marked
        return
    endif

    let used = getmarklist()->map({_, m -> m.mark[1]})
    let available = g:marks_maps['color']->keys()->filter({_, m -> used->index(m) < 0})
    if len(available) == 0
        echoerr 'No available colors left'
        return
    endif

    exe 'mark ' .. available[0]
endfunction
nnoremap m<cr> <cmd>call MarkWithColor()<cr>
nnoremap m<bs> :<c-u>marks<cr>:delmarks<space>
" end of Marks }}}

" Window and tab management
function! s:wincmd(key)
    let curwin = winnr()
    exec 'wincmd ' .. a:key
    if curwin != winnr()
        return
    endif

    if match(a:key, '[jk]') + 1
        wincmd s
    elseif match(a:key, '[hl]') + 1
        wincmd v
    endif
    exec 'wincmd ' .. a:key
endfunction
nnoremap <c-right> <cmd>call <sid>wincmd('l')<cr>
nnoremap <c-left> <cmd>call <sid>wincmd('h')<cr>
nnoremap <c-down> <cmd>call <sid>wincmd('j')<cr>
nnoremap <c-up> <cmd>call <sid>wincmd('k')<cr>
nnoremap <c-pagedown> <cmd>close<cr>
nnoremap <c-w>t <cmd>tab split<cr>
nnoremap <c-w>c <cmd>tabclose<cr>
nnoremap <c-w>O <cmd>tabonly<cr>
nnoremap <c-w>, <c-w>H
nnoremap <c-w>a <c-w>J
nnoremap <c-w>e <c-w>K
nnoremap <c-w>i <c-w>L

" Search and replace {{{

" Substitute
" Without prefilled pattern
nnoremap gss :%s///g<left><left><left>
" Word under cursor
nnoremap gsw :%s/\<<c-r><c-w>\>//g<left><left>
" Last search pattern
nnoremap gs/ :%s///g<left><left>
" From default register
nnoremap gs" :%s/<c-r>"//g<left><left>
" Visually selected text
xnoremap gs y:<c-u>%s/<c-r>"//g<left><left>

" Change word under cursor
nnoremap c* *``cgn<cmd>set hlsearch<cr>
nnoremap c# #``cgN<cmd>set hlsearch<cr>

function! s:is_cursor_at_start_of_word(query) abort
    let curcol = col('.') - 1
    return a:query ==# getline('.')[curcol: curcol + len(a:query) - 1]
endf

function! s:search_word(forward, ignore_case, match_boundaries) abort
    let query = expand('<cword>')
    if empty(query)
        echohl ErrorMsg
        echomsg "E348: No string under cursor"
        echohl None
        return
    endif

    " Doing * on a nonword character at end of line produces no word
    " characters so matching boundaries is invalid.
    let pattern =
\          (a:match_boundaries && query =~# '\w' ? '\<' : '')
\       ..  query
\       .. (a:match_boundaries && query =~# '\w' ? '\>' : '')
\       .. (a:ignore_case ? '\c' : '\C')
    call setreg('/', pattern)

    let next = a:forward ? 'n' : 'N'

    if a:forward || s:is_cursor_at_start_of_word(expand('<cword>'))
        exe 'normal! ' .. next
    else
        exe 'normal! ' .. next .. next
    endif
endf

" Same as *, g*, # and g#, but always match case
nnoremap <silent> * <cmd>call <sid>search_word(1, 0, 1) \| let v:searchforward = 1<cr>
nnoremap <silent> # <cmd>call <sid>search_word(0, 0, 1) \| let v:searchforward = 0<cr>
nnoremap <silent> g* <cmd>call <sid>search_word(1, 0, 0) \| let v:searchforward = 1<cr>
nnoremap <silent> g# <cmd>call <sid>search_word(0, 0, 0) \| let v:searchforward = 0<cr>
xnoremap <silent> * y/\V\C<c-r>"<cr>
xnoremap <silent> # y?\V\C<c-r>"<cr>

" Same as *, g*, # and g#, but always ignore case
nnoremap <silent> <leader>* <cmd>call <sid>search_word(1, 1, 1) \| let v:searchforward = 1<cr>
nnoremap <silent> <leader># <cmd>call <sid>search_word(0, 1, 1) \| let v:searchforward = 0<cr>
nnoremap <silent> <leader>g* <cmd>call <sid>search_word(1, 1, 0) \| let v:searchforward = 1<cr>
nnoremap <silent> <leader>g# <cmd>call <sid>search_word(0, 1, 0) \| let v:searchforward = 0<cr>
xnoremap <silent> <leader>* y/\V\c<c-r>"<cr>
xnoremap <silent> <leader># y?\V\c<c-r>"<cr>

" Next incremental search match with Tab
cnoremap <expr> <tab>   get({'/': "\<c-g>", '?': "\<c-t>"}, getcmdtype()) ?? "<c-z>"
cnoremap <expr> <s-tab> get({'/': "\<c-t>", '?': "\<c-g>"}, getcmdtype()) ?? "<s-tab>"

" Toggle search highlighting
nnoremap <leader>h <cmd>set hlsearch!<cr>
" end of Search and Replace }}}

" Quickfix/location list management
nnoremap <leader>lq <cmd>lclose<bar>cclose<cr>
nnoremap <leader>q, <cmd>QuickfixAddCurrent<cr>
nnoremap <leader>l, <cmd>LocListAddCurrent<cr>
nnoremap <leader>lF :lfile ~/.vim/lists/
nnoremap <leader>qF :cfile ~/.vim/lists/

" File navigation
nnoremap <leader>ee :edit <c-r>=expand('%:.:h') .. '/'<cr>
nnoremap <leader>ex :split <c-r>=expand('%:.:h') .. '/'<cr>
nnoremap <leader>ev :vertical split <c-r>=expand('%:.:h') .. '/'<cr>
nnoremap <leader>et :tabedit <c-r>=expand('%:.:h') .. '/'<cr>
nnoremap <leader>ff :edit **/
nnoremap <leader>fx :split **/
nnoremap <leader>fv :vertical split **/
nnoremap <leader>ft :tabedit **/

function! ListOldFiles() abort
    return v:oldfiles[:10]
\       ->mapnew({idx, filename -> (idx + 1) .. "\t" .. filename .. "\t" .. (idx + 1)})
\       ->join("\n")
endfunction
nnoremap <leader>O :echo ListOldFiles()<cr>:edit #<

" Buffer navigation
nnoremap <leader>b :buffers<cr>:buffer<space>
nnoremap <leader><s-tab> :buffers t<cr>:filter // buffers t <bar> call feedkeys(':buffer ')<home><c-right><c-right><left>
nnoremap <leader><c-g> <cmd>let @@=bufnr()<cr>

" Tags navigation
nnoremap <leader>t <cmd>tag<cr>
nnoremap <leader>T :tjump /<c-d>

" Yanking to and pasting from clipboard
nnoremap <leader>p "+
xnoremap <leader>p "+
inoremap <c-r><c-d> <c-r>+
cnoremap <c-r><c-d> <c-r>+
nnoremap <leader>y "+y
xnoremap <leader>y "+y
nnoremap <leader>Y "+y$

" Remove operator
nnoremap x "_d
xnoremap x "_d
nnoremap X "_D
nnoremap xx "_d_

" Show the syntax highlight group under cursor
nnoremap <F10> <cmd>echo
\         'hi<' .. synIDattr(           synID(line('.'), col('.'), 1),  'name') .. '> '
\   .. 'trans<' .. synIDattr(           synID(line('.'), col('.'), 0),  'name') .. '> '
\   ..    'lo<' .. synIDattr(synIDtrans(synID(line('.'), col('.'), 1)), 'name') .. '>'
\<cr>

nnoremap <leader>u <cmd>silent w !urlview<cr>
nnoremap <leader>d <cmd>w !diff -u % - \| color-diff<cr>

inoreabbrev teh the
inoreabbrev wihch which
inoreabbrev waht what

" Load vimrc TOC into location list.
augroup VimrcMappings
    au!
    autocmd BufEnter vimrc nnoremap <buffer> <localleader>t <cmd>lvimgrep /{{{/j %<cr>
    " Workaround: close the incorrectly inferred fold from the above line }}}
augroup END

nnoremap <2-LeftMouse> zf%

nnoremap <leader>>ql <cmd>QuickfixToLocList<cr>
nnoremap <leader>>lq <cmd>LocListToQuickfix<cr>
nnoremap <leader>>tq <cmd>TagStackToQuickfix<cr>
nnoremap <leader>>aq <cmd>ArgsToQuickfix<cr>
nnoremap <leader>>ml <cmd>call MarksToLocList() \| lopen<cr>

inoremap <pagedown> <c-^>
cnoremap <pagedown> <c-^>

" }}}
" }}}

" Autocommands {{{
" Disable Command-line window welcome message
autocmd! vimHints CmdwinEnter

function! s:mkdir(path)
    if a:path =~? '^fugitive://'
        return
    endif
    call mkdir(a:path, 'p')
endfunction

augroup VimrcAutocommands
    autocmd!
    autocmd BufWritePre,FileWritePre * silent! call s:mkdir(expand('<afile>:p:h'))
    autocmd TabLeave * cclose | lclose
    autocmd VimResized * exe "wincmd ="
    autocmd CmdlineEnter /,\? set hlsearch
    autocmd CmdlineLeave /,\? set nohlsearch

    " Using plugins
    autocmd TerminalOpen * HideBadWhitespace
augroup END
" }}}

" Plugins {{{
let s:light_mode = getenv('VIM_LIGHT') != v:null && getenv('VIM_LIGHT') != 'false'

" Load later
packadd! matchit
" Load immediately
packadd minpac
packadd vim-mycolors

call minpac#init()

call minpac#add('k-takata/minpac', #{type: 'opt'})

call minpac#add('EgZvor/vim-bad-whitespace', #{branch: 'add-augroups'})
call minpac#add('EgZvor/vim-unimpaired', #{branch: 'egzvor'})
call minpac#add('AndrewRadev/inline_edit.vim')
call minpac#add('AndrewRadev/splitjoin.vim')
call minpac#add('AndrewRadev/quickpeek.vim')
call minpac#add('flwyd/vim-conjoin')
call minpac#add('girishji/scope.vim')
call minpac#add('honza/vim-snippets')
call minpac#add('jeetsukumaran/vim-indentwise')
call minpac#add('justinmk/vim-dirvish')
call minpac#add('Konfekt/FastFold')
call minpac#add('machakann/vim-sandwich')
call minpac#add('markonm/traces.vim')
call minpac#add('RRethy/vim-illuminate')
call minpac#add('simnalamburt/vim-mundo')
call minpac#add('SirVer/ultisnips')
call minpac#add('stefandtw/quickfix-reflector.vim')
call minpac#add('tommcdo/vim-exchange')
call minpac#add('tpope/vim-abolish')
call minpac#add('tpope/vim-commentary')
call minpac#add('tpope/vim-eunuch')
call minpac#add('tpope/vim-obsession')
call minpac#add('tpope/vim-repeat')
call minpac#add('thinca/vim-prettyprint')

" Syntax plugins
call minpac#add('aklt/plantuml-syntax')
call minpac#add('dag/vim-fish')
call minpac#add('elixir-editors/vim-elixir')
call minpac#add('fladson/vim-kitty')
call minpac#add('jidn/vim-dbml')
call minpac#add('lifepillar/pgsql.vim')

" Language plugins
call minpac#add('gleam-lang/gleam.vim')
call minpac#add('jeetsukumaran/vim-pythonsense')

" Git stuff
call minpac#add('joechrisellis/vim-git-arglist')
call minpac#add('junegunn/gv.vim')
call minpac#add('rbong/vim-flog')
call minpac#add('tommcdo/vim-fugitive-blame-ext')
call minpac#add('tpope/vim-fugitive')
call minpac#add('whiteinge/diffconflicts')

" fzf has slow ui
call minpac#add('srstevenson/vim-picker')

" Color schemes are found in opt too
call minpac#add('lifepillar/vim-solarized8', #{type: 'opt'})
call minpac#add('romainl/vim-dichromatic', #{type: 'opt'})

" Load only for Rust
call minpac#add('rust-lang/rust.vim', #{type: 'opt', for: ['rust'], config: 'optional/rust/rust.vim'})

" Load only for development
let s:dev_filetypes = ['c', 'go', 'javascript', 'python', 'rust', 'sh', 'haskell']
call minpac#add('dense-analysis/ale', #{
\   type: 'opt', for: s:light_mode ? [] : s:dev_filetypes, config: 'optional/ale.vim'})
call minpac#add('ycm-core/YouCompleteMe', #{
\   type: 'opt', for: s:light_mode ? [] : s:dev_filetypes, config: 'optional/youcompleteme.vim'})
call minpac#add('vim-test/vim-test', #{type: 'opt', for: s:dev_filetypes, config: 'optional/test.vim'})

" Rarely used
call minpac#add('junegunn/goyo.vim', #{type: 'opt'})
call minpac#add('junegunn/vim-easy-align', #{type: 'opt', config: 'optional/easy-align.vim'})
" }}}

" Plugin settings {{{

" Use this group for setup()-based settings.
" au PluginSettings VimEnter * cmd
augroup PluginSettings | au! | augroup END

" auto-git-diff
let g:auto_git_diff_command_options = '--no-ext-diff'

" switch.vim
let g:switch_mapping = ''
let g:switch_no_builtins = 1
let g:switch_find_smallest_match = 1

" marks-maps.vim
let g:marks_maps = #{
\   color: #{R: '1', S: '2', N: '3', D: '4', B: '5'},
\}

" prettyprint.vim
let g:prettyprint_width = 120

" vim-black
let g:black_virtualenv = '~/.local/pipx/venvs/black'

" pgsql.vim
let g:sql_type_default = 'pgsql'

" YouCompleteMe
let g:ycm_autoclose_preview_window_after_insertion=1
let g:ycm_collect_identifiers_from_tags_files=1
let g:ycm_show_diagnostics_ui = 0
let g:ycm_echo_current_diagnostic = 0
let g:ycm_filetype_specific_completion_to_disable = #{
\   notes: 1,
\   markdown: 1,
\   rst: 1,
\   selectprompt: 1
\}
let g:ycm_filetype_blacklist = {
\   'markdown': 1,
\   'notes': 1,
\   'markdown.notes': 1,
\   'selectprompt': 1,
\   'sql': 1,
\}
let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'
let g:ycm_confirm_extra_conf = 0
let g:ycm_auto_hover = ''
let g:ycm_clangd_binary_path = '/usr/bin/clangd'
let g:ycm_gopls_binary_path = $HOME .. '/go/bin/gopls'
let g:ycm_gopls_args = ['-remote=auto']

" ALE {{{
let g:ale_set_signs = 0

let g:ale_lint_on_enter = 1
let g:ale_lint_on_filetype_changed = 1
let g:ale_lint_on_save = 1
let g:ale_lint_on_insert_leave = 1
let g:ale_lint_on_text_changed = 0
let g:ale_virtualtext_cursor = 'all'

let g:ale_fix_on_save = 1

let g:ale_set_loclist = 0

let g:ale_pattern_options = {
\   '.*\.env$': #{ale_enabled: 0},
\   '^fugitive:/.*': #{ale_enabled: 0},
\}

let g:ale_linters = #{
\   python: ['ruff'],
\   bash: ['shellcheck'],
\   eruby: ['erubi'],
\   go: ['gobuild', 'govet', 'gofmt', 'golangci-lint', 'gopls'],
\   yaml: [],
\}
let g:ale_fixers = #{
\   go: [
\       'gofmt',
\       'remove_trailing_lines',
\       'trim_whitespace',
\       'goimports',
\   ],
\   gleam: ['gleam_format'],
\}

let g:ale_python_pylint_options = '--init-hook="import sys; sys.path.append(\".\")" --disable=W --disable=C'
let g:ale_python_ruff_options = '--ignore E501,E402'
let g:ale_c_clangtidy_checks = ['*', '-*braces-around-statements,-android-cloexec-open']
let g:ale_c_clang_options = '-std=gnu99 -Wall -D _POSIX_SOURCE=2'
let g:ale_c_gcc_options = '-std=gnu99 -Wall'
let g:ale_go_golangci_lint_options = '--fast'
let g:ale_go_golines_options = '--max-length=90'
let g:ale_go_gopls_options = '-remote=auto'
let g:ale_sql_sqlfluff_options = '--exclude=L003,L004,L016,L039'

" }}}

" rust.vim
let g:rustfmt_emit_files = 1

" UltiSnips
let g:UltiSnipsExpandTrigger = '<c-b>'
let g:UltiSnipsJumpForwardTrigger = '<c-j>'
let g:UltiSnipsJumpBackwardTrigger = '<c-k>'

" vim-test
function! s:copy_strategy(cmd)
    call popup_notification(a:cmd, #{pos: "center"})
    call setreg('+', a:cmd)
endfunction
let g:test#custom_strategies = {'copy': function('s:copy_strategy')}
let g:test#strategy = 'copy'
let g:test#python#pytest#options = '-s -ra'

" disable netrw
let g:loaded_netrwPlugin = 1

" disable matchparen
let g:loaded_matchparen = 1

" markdown
let g:markdown_folding = 1

" rst
let g:rst_fold_enabled = 1

" vimproviser
let g:vimproviser_skip_trigger_registration = 1
let g:vimproviser_pairs = #{
\   AleErrors: ['<plug>(ale_previous)', '<plug>(ale_next)'],
\   Buffers: ['[b', ']b'],
\   Chunks: ['[c', ']c'],
\   Jumps: ['<c-o>', '<c-i>'],
\   Marks: ['[`', ']`'],
\   JumpFile: ['go', 'gi'],
\   Search: ['N', 'n'],
\   TimeTravel: ['g-', 'g+'],
\   Undo: ['u', '<c-r>'],
\   Scroll: ['<c-u>', '<c-d>'],
\   ZaglavF: ['F<cr>', 'f<cr>'],
\   ZaglavT: ['T<cr>', 't<cr>'],
\}
let g:vimproviser_triggers = #{
\   ArgList: ['[a', ']a'],
\   Buffers: ['[b', ']b'],
\   Chunks: ['[c', ']c'],
\   LocationList: ['[l', ']l'],
\   LocationListFile: ['[<c-l>', ']<c-l>'],
\   JumpFile: ['<c-6>', '<c-o>', '<c-i>', '<bs>'],
\   Marks: ["['", "]'"],
\   Macros: ['@h', '@l'],
\   Quickfix: ['[q', ']q'],
\   QuickfixFile: ['[<c-q>', ']<c-q>'],
\   TimeTravel: ['g-', 'g+'],
\   Undo: ['u', '<c-r>'],
\}

" Illumination
let g:Illuminate_ftwhitelist = ['python', 'ts', 'c', 'cpp', 'sh', 'go']
let g:Illuminate_ftHighlightGroups = {
\   'python:blacklist': ['Statement', 'Constant', 'Comment'],
\   'go:blacklist':     ['Statement', 'Constant', 'Comment'],
\}

" Obsession
let g:obsession_no_bufenter=1

" Traces
let g:traces_num_range_preview=1

" Tagbar
let g:tagbar_map_help = '<F1>'

" mundo
let g:mundo_preview_bottom = 1

" }}}

colorscheme solarized8

if &term ==? 'xterm-kitty'
    call kitty#fix_terminal()
    call kitty#default_terminal_background()
endif

" TODO: local global marks?? Jump to a marked file from the current file.
" Each file references multiple others.
" Jump to definition -> save "portal" labeled with function name.
" Fuzzy find a file -> save "portal" labeled with file name.
" b:portals = {label: path} .
" after/filetype/go.vim -- b:portals['test_source'] = '_test.go'
" Assign a key to each portal (4-8 keys).
" "portal" mode. ctrl-aeih - hold ctrl quickly jump through multiple files to
" reach a destination. ctrl down - aaeahiaa - ctrl up
" TODO: jump to last accessed test file, api file and so on.
" TODO: put clipboard into Vim server and parse it with errorformat to find file paths (set efm+=%f).
" TODO: buflist: persist in viminfo most_visited and last_modified. Add reset.
" TODO: finish writing about vim clipboard.
"       - frontend: select OS/(Neo)Vim -> modify content

" vim: foldmethod=marker foldlevel=0
