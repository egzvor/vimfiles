if exists('g:loaded_textobjects')
    finish
endif

let g:loaded_textobjects = 1

xnoremap <silent> ii :<c-u>call textobjects#inIndentation()<cr>
onoremap <silent> ii :<c-u>call textobjects#inIndentation()<cr>

" 26 simple text objects
for char in [ '_', '.', ':', ',', ';', '<bar>', '/', '<bslash>', '*', '+', '-', '#', '`' ]
        execute 'xnoremap i' . char . ' :<c-u>normal! T' . char . 'vt' . char . '<cr>'
        execute 'onoremap i' . char . ' :normal vi' . char . '<cr>'
        execute 'xnoremap a' . char . ' :<c-u>normal! F' . char . 'vf' . char . '<cr>'
        execute 'onoremap a' . char . ' :normal va' . char . '<cr>'
endfor

xnoremap . `[o`]
onoremap . <cmd>normal V.<cr>
