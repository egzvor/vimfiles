let s:noswapfile = (2 == exists(':noswapfile')) ? 'noswapfile' : ''

function! s:init_buffer(filetype)
    exe 'setfiletype ' .. a:filetype
    setlocal nobuflisted
    setlocal buftype=nofile noswapfile
    setlocal bufhidden=wipe
endfunction

function! BuflistBuffer() abort
    let lines = []

    let lines = lines->add("===Last used buffers===")
    let lines = lines->extend(buflist#lastused#get()
\       ->copy()
\       ->reverse()
\       ->buflist#without_current_buffer()
\       ->buflist#without_hidden_buffers()
\       ->buflist#without_terminal_buffers()
\       [:9]
\       ->buflist#format_buffer_names())

    let lines = lines->add("===Most visited buffers===")
    let lines = lines->extend(buflist#freq#most_visited_buffers()
\       ->copy()
\       ->buflist#without_current_buffer()
\       ->buflist#without_hidden_buffers()
\       ->buflist#without_terminal_buffers()
\       [:9]
\       ->buflist#format_buffer_names())

    let lines = lines->add("===Last modified buffers===")
    let lines = lines->extend(buflist#modified#get()
\       ->buflist#without_current_buffer()
\       ->buflist#without_hidden_buffers()
\       ->buflist#without_terminal_buffers()
\       [:9]
\       ->buflist#format_buffer_names())

    execute 'silent' s:noswapfile 'keepalt botright' len(lines) 'split buflist://last'
    call s:init_buffer('buflist')
    " TODO: reuse buffer?
    " silent keepmarks keepjumps %delete _
    silent keepmarks keepjumps call setline(1, lines)
endfunction
