if exists('g:loaded_buflist')
    finish
endif

let g:loaded_buflist = 1

function! ListBuffers() abort
    echohl Title
    echo "=== Last used buffers ==="
    echohl None
    echo buflist#lastused#get()
\       ->copy()
\       ->reverse()
\       ->buflist#without_current_buffer()
\       ->buflist#without_hidden_buffers()
\       ->buflist#without_terminal_buffers()
\       [:9]
\       ->buflist#format_buffer_names()
\       ->join("\n")
\       .. "\n"
    echohl Title
    echo "=== Most visited buffers ==="
    echohl None
    echo buflist#freq#most_visited_buffers()
\       ->copy()
\       ->buflist#without_current_buffer()
\       ->buflist#without_hidden_buffers()
\       ->buflist#without_terminal_buffers()
\       [:9]
\       ->buflist#format_buffer_names()
\       ->join("\n")
\       .. "\n"
    echohl Title
    echo "=== Last modified buffers ==="
    echohl None
    echo buflist#modified#get()
\       ->buflist#without_current_buffer()
\       ->buflist#without_hidden_buffers()
\       ->buflist#without_terminal_buffers()
\       [:9]
\       ->buflist#format_buffer_names()
\       ->join("\n")
\       .. "\n"
endfunction

function LastModifiedBuffer() abort
    let last_modified = buflist#modified#get()
\       ->buflist#without_hidden_buffers()
\       ->buflist#without_current_buffer()
\       ->buflist#without_terminal_buffers()

    if len(last_modified) == 0
        echohl ErrorMsg
        echo 'No other file has been modified yet'
        echohl None
        return
    endif

    return last_modified[0].bufnr
endfunction

augroup Buflist
    au!
    autocmd BufEnter *
\       if !buflist#freq#is_accessed_recently()
\     |     call buflist#freq#increment_frequency()
\     | endif
    autocmd BufWritePost * call buflist#modified#push()
augroup END

command -nargs=0 Freq echo buflist#freq#frequencies()
