const s:ACCESSED_TIMES_THRESHOLD = 10

" TODO: add more conditions:
" - modified more than X times
" I save very often.
" const MODIFIED_TIMES_THRESHOLD = 10

augroup BuflistColorMark
    autocmd BufEnter * if s:should_be_colored() | silent! call MarkWithColor() | endif
augroup END

function! s:should_be_colored() abort
    return buflist#freq#get_frequency() > s:ACCESSED_TIMES_THRESHOLD
endfunction
