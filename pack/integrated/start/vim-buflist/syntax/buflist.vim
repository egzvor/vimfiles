" Common
syn match Title /===.*/

" Go - Work
syn match TabLineSel #/\zsgen\ze/\f\+.go#
syn match TabLineSel #/\zsgenerated\ze/\f\+.go#
syn match DiffText #/\zs\(repositories\|storage\|queries\)\ze/#
syn match DiffChange #/\zsapi\ze/#
syn match Underlined #/\zsmodels\ze/#
syn match Underlined #/\zsprocessors\ze/#
syn match DiffAdd #.*/\zs.*_test.go\>#
syn match DiffAdd #\<tests\?\>#

" Python
syn match DiffAdd #/\zstest_.*.py\>#
syn match TabLineSel #/usr/lib.*site-packages#

for [m, i] in items(g:marks_maps['color'])
    let [bufnr, lnum, col, off] = getpos("'" .. m)
    if [bufnr, lnum, col, off] == [0, 0, 0, 0]
        continue
    endif
    exe 'syn match MarkColor' .. i $'#^{bufnr}\>#'
endfor
