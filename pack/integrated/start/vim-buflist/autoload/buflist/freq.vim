let s:buflist_frequencies = {}

function! buflist#freq#frequencies() abort
    return s:buflist_frequencies
\       ->copy()
\       ->filter({bufnr, _ -> buflisted(str2nr(bufnr))})
endfunction

function buflist#freq#get_frequency(bufnr = 0) abort
    return get(s:buflist_frequencies, a:bufnr ?? bufnr(), 0)
endfunction

function! buflist#freq#most_visited_buffers() abort
    let bufnr_list = buflist#freq#frequencies()
\       ->keys()
\       ->sort({first, second -> buflist#freq#get_frequency(first) < buflist#freq#get_frequency(second)})

    let result = []
    for bufnr in bufnr_list
        let info = getbufinfo(str2nr(bufnr))
        if len(info) > 0
            call add(result, info[0])
        endif
    endfor

    return result
endfunction

function! buflist#freq#increment_frequency() abort
    let buf_info = getbufinfo('%')[0]
    if buf_info.listed
        let s:buflist_frequencies[buf_info.bufnr] = get(
\           s:buflist_frequencies, buf_info.bufnr, 0
\       ) + 1
    endif
endfunction

" Returns true if the current buffer was accessed recently.
function! buflist#freq#is_accessed_recently() abort
    return localtime() - getbufinfo('%')[0].lastused < 20
endfunction

function! buflist#freq#reset() abort
    let s:buflist_frequencies = {}
endfunction
