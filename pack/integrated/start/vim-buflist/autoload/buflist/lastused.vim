function! buflist#lastused#cmp(buf1, buf2) abort
    if a:buf1.lastused > a:buf2.lastused
        return 1
    elseif a:buf1.lastused < a:buf2.lastused
        return -1
    else
        return 0
    endif
endfunction

function! buflist#lastused#get() abort
    let bufinfo = getbufinfo(#{buflisted: 1})
    call sort(bufinfo, function('buflist#lastused#cmp'))
    return bufinfo->filter({_, info -> info['bufnr'] != bufnr()})
endfunction
