" Element with index 0 is the last added element.
let s:last_modified_queue = []

function buflist#modified#push() abort
    let current = getbufinfo('%')[0]
    let s:last_modified_queue = s:last_modified_queue
\       ->copy()
\       ->s:refresh()
\       ->buflist#without_current_buffer()
\       ->buflist#without_hidden_buffers()
\       ->buflist#without_terminal_buffers()
\       ->insert(current->extend(#{lastline: getline('.')}))
\       [:50]
endfunction

function buflist#modified#get() abort
    return s:last_modified_queue
\       ->copy()
\       ->s:refresh()
endfunction

function s:refresh(in) abort
    return a:in->filter({_, buf -> len(getbufinfo(buf.bufnr)) > 0})
endfunction
