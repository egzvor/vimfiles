function! buflist#format_buffer_names(buf_list) abort
    return a:buf_list->map(function('buflist#format_buffer'))
endfunction

function! buflist#format_buffer(_, buf) abort
    return printf(
    \       "%3d%s	%s	%s	%s",
    \       a:buf.bufnr,
    \       a:buf.changed ? ' +': '',
    \       fnamemodify(a:buf.name, ":.:p"),
    \       a:buf.bufnr,
    \       get(a:buf, 'lastline', ''),
    \   )
    \   ->trim()
endfunction

function! buflist#without_current_buffer(in) abort
    return a:in->filter({_, buf -> buf.bufnr != bufnr()})
endfunction

function! buflist#without_hidden_buffers(in) abort
    return a:in->filter({idx, buf -> buf.bufnr != 0 && buf.name !=# ''})
endfunction

function! buflist#without_terminal_buffers(in) abort
    return a:in->filter({_, buf -> term_list()->index(buf.bufnr) == -1})
endfunction

function! buflist#idx() abort
    return matchstr(getline('.'), '\d\+')
endfunction
