function s:highlight(group, opts = {}) abort
    execute 'highlight ' .. a:group .. ' ' .. join(
\       [
\           'term=' .. get(a:opts, 'term', 'NONE'),
\           'cterm=' .. get(a:opts, 'cterm', 'NONE'),
\           'ctermbg=' .. get(a:opts, 'ctermbg', 'NONE'),
\           'ctermfg=' .. get(a:opts, 'ctermfg', 'NONE'),
\           'gui=' .. get(a:opts, 'gui', 'NONE'),
\           'guibg=' .. get(a:opts, 'guibg', 'NONE'),
\           'guifg=' .. get(a:opts, 'guifg', 'NONE'),
\           'guisp=' .. get(a:opts, 'guisp', 'NONE'),
\       ],
\       ' '
\   )
endfunction

function s:extend_opt(opt, value) abort
    return a:opt->split(',')->add(a:value)->join(',')
endfunction

function s:add_attr(attrs, value) abort
    return extend(a:attrs, #{
\       term: a:attrs->get('term', '')->s:extend_opt(a:value),
\       cterm: a:attrs->get('cterm', '')->s:extend_opt(a:value),
\       gui: a:attrs->get('gui', '')->s:extend_opt(a:value),
\   })
endfunction

function s:bold(attrs) abort
    return s:add_attr(a:attrs, 'bold')
endfunction

function s:italic(attrs) abort
    return s:add_attr(a:attrs, 'italic')
endfunction

function s:underline(attrs) abort
    return s:add_attr(a:attrs, 'underline')
endfunction

function mycolors#my_solarized_highlights() abort
    call s:highlight('Comment', s:italic(#{ctermfg: '96'}))
    call s:highlight('DiffAdd', #{ctermbg: '194', guibg: '#d7ffd7'})
    call s:highlight('DiffChange', #{ctermbg: '230', guibg: '#ffffd7'})
    call s:highlight('DiffDelete', #{ctermfg: '217', ctermbg: '224', guifg: '#ffafaf', guibg: '#ffd7d7'})
    call s:highlight('DiffText', #{ctermbg: '195', guibg: '#d7ffff'})
    call s:highlight('diffAdded', #{ctermfg: '2'})
    call s:highlight('diffChanged', #{ctermfg: '3'})
    call s:highlight('diffRemoved', #{ctermfg: '1'})
    call s:highlight('diffLine', #{ctermbg: '195', guibg: '#d7ffff'})
    call s:highlight('GitGutterAdd', s:bold(#{ctermfg: '34'}))
    call s:highlight('GitGutterChange', s:bold(#{ctermfg: '91'}))
    call s:highlight('GitGutterDelete', s:bold(#{ctermfg: '204'}))
    call s:highlight('Folded', s:bold(#{ctermbg: '15', ctermfg: '11'}))
    call s:highlight('MatchParen', s:bold(#{ctermfg: '168', ctermbg: '180'}))
    call s:highlight('QuickFixLine', #{guibg: '#f4eedb'})
    call s:highlight('Search', #{guibg: '#87d7af'})
    call s:highlight('IncSearch', #{cterm: 'inverse', guifg: '#005f5f'})
    call s:highlight('CurSearch', #{cterm: 'inverse', guifg: '#005f5f'})
    call s:highlight('SignColumn', #{ctermbg: '7'})
    call s:highlight('SpellBad', s:italic(s:underline(#{ctermfg: 'red'})))

    let statusline = #{ctermfg: '0', ctermbg: '7', guifg: '#073642', guibg: '#eee8d5'}
    let statusline_nc = #{ctermfg: '246', ctermbg: '7', guifg: '#949494', guibg: '#eee8d5'}
    call s:highlight('StatusLine', statusline)
    call s:highlight('StatusLineNC', statusline_nc)
    call s:highlight('MarkColor1', #{ctermfg: '7', ctermbg: '232', guifg: '#eee8d5', guibg: '#080808'})
    call s:highlight('MarkColor2', statusline->extend(#{ctermbg: '153', guibg: '#afd7ff'}))
    call s:highlight('MarkColor3', statusline->extend(#{ctermbg: '194', guibg: '#d7ffd7'}))
    call s:highlight('MarkColor4', statusline->extend(#{ctermbg: '224', guibg: '#ffd7d7'}))
    call s:highlight('MarkColor5', statusline->extend(#{ctermbg: '189', guibg: '#d7d7ff'}))
    call s:highlight('MarkColorNC1', #{ctermfg: '7', ctermbg: '232', guifg: '#a8a8a8', guibg: '#080808'})
    call s:highlight('MarkColorNC2', statusline_nc->extend(#{ctermbg: '153', guibg: '#afd7ff'}))
    call s:highlight('MarkColorNC3', statusline_nc->extend(#{ctermbg: '194', guibg: '#d7ffd7'}))
    call s:highlight('MarkColorNC4', statusline_nc->extend(#{ctermbg: '224', guibg: '#ffd7d7'}))
    call s:highlight('MarkColorNC5', statusline_nc->extend(#{ctermbg: '189', guibg: '#d7d7ff'}))
    call s:highlight('MarkColorPrefix1', #{guibg: '#eee8d5', guifg: '#080808'})
    call s:highlight('MarkColorPrefix2', #{guibg: '#eee8d5', guifg: '#afd7ff'})
    call s:highlight('MarkColorPrefix3', #{guibg: '#eee8d5', guifg: '#d7ffd7'})
    call s:highlight('MarkColorPrefix4', #{guibg: '#eee8d5', guifg: '#ffd7d7'})
    call s:highlight('MarkColorPrefix5', #{guibg: '#eee8d5', guifg: '#d7d7ff'})

    call s:highlight('TabLine', #{ctermfg: '246', ctermbg: '7', guifg: '#949494', guibg: '#eee8d5'})
    call s:highlight('TabLineFill', #{ctermfg: '11', ctermbg: '7', guifg: '#657b83', guibg: '#eee8d5'})
    call s:highlight('TabLineSel', #{ctermfg: '0', ctermbg: '7', guifg: '#073642', guibg: '#eee8d5'})
    call s:highlight('VertSplit', #{ctermfg: '254', guifg: '#eee8d5'})
    call s:highlight('illuminatedWord', #{guibg: '#f4eedb'})
    call s:highlight('Pmenu', #{ctermbg: '7'})
    call s:highlight('PmenuSel', #{guibg: '#87d7af'})
    call s:highlight('PmenuSbar', #{ctermbg: '7'})
    call s:highlight('PmenuThumb', #{guibg: '#005f5f'})
    :highlight clear Visual
    call s:highlight('Visual', #{ctermbg: '189', ctermfg: '10'})
    call s:highlight('Cursor', #{guifg: '#ffaaaa'})
    call s:highlight('HighlightedyankRegion', #{ctermbg: '230', guibg: '#ffffd7'})
    call s:highlight('HighlightedyankRegionClipboard', #{ctermbg: '224', guibg: '#ffd7d7'})
    call s:highlight('VimproviserStatus', s:bold(#{ctermfg: '5', guifg: '#cb4b16'}))
    call s:highlight('QuickScopePrimary', s:bold(#{ctermfg: 'red'}))
    call s:highlight('QuickScopeSecondary', s:bold(#{ctermfg: 'black'}))
    call s:highlight('UnderCursorMatch', s:italic(#{guifg: '#000000', guibg: '#ffffff'}))
    call s:highlight('lspInlayHintsParameter', #{guibg: '#faf3e0', guifg: '#bfb9a7'})
    call s:highlight('YcmInlayHint', #{guibg: '#faf3e0', guifg: '#bfb9a7'})
    call s:highlight('Bleak', #{ctermfg: '254', guifg: '#eee8d5'})
endfunction
