augroup MyColors
    autocmd!
    autocmd ColorScheme solarized,solarized8,solarized8_flat,solarized8_high call mycolors#my_solarized_highlights()
augroup END
