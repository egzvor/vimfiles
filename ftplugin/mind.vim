setlocal sw=4
nnoremap <buffer> o o-<space>
nnoremap <buffer> O o- <c-t>
nnoremap <buffer> <localleader>n /^\s*-/e<cr>
nnoremap <buffer> <localleader>N ?^\s*-?e<cr>
