if exists('g:loaded_after_fugitive')
    finish
endif

let g:loaded_after_fugitive = 1

if exists(':G')
nnoremap <leader>gg :G<space>
nnoremap <leader>gb <cmd>G blame<cr>
nnoremap <leader>gp :G push<space>
if exists(':VimproviserTrigger')
    nnoremap <leader>gd <cmd>aboveleft Gvdiffsplit HEAD~<bar>wincmd p<bar>VimproviserTrigger Chunks<cr>
    nnoremap <leader>gD :aboveleft Gvdiffsplit  <bar>wincmd p<bar>VimproviserTrigger Chunks<s-left><s-left><s-left><s-left>
else
    nnoremap <leader>gd <cmd>aboveleft Gvdiffsplit HEAD~<bar>wincmd p<cr>
    nnoremap <leader>gD :aboveleft Gvdiffsplit  <bar>wincmd p<s-left><s-left><left>
endif
nnoremap <leader>ge <cmd>Gedit<cr>
nnoremap <leader>gE :Gedit<space>
nnoremap <leader>gr :G rebase<space>
nnoremap <leader>gR <cmd>G reflog<cr>
nnoremap <leader>gh <cmd>G log --decorate=full --oneline -100<cr>
nnoremap <leader>gt <cmd>Git difftool HEAD~<cr>
nnoremap <leader>gT :Git difftool<space>
nnoremap <leader>gf <cmd>G fetch<cr>
nnoremap <leader>go <cmd>tab term ++close gitui<cr>
nnoremap <leader>gS <cmd>tab term ++shell GIT_PAGER='delta --paging=never' git show<cr>
nnoremap <leader>gca <cmd>G commit --amend<cr>

function FugitiveToggle() abort
  try
    exe filter(getwininfo(), "get(v:val['variables'], 'fugitive_status', v:false) != v:false")[0].winnr .. "wincmd c"
  catch /E684/
    Git
  endtry
endfunction
nnoremap <leader>gs <cmd>call FugitiveToggle()<CR>
endif

if exists(':GlistDiff')
    nnoremap <leader>gl :GlistDiff<cr>
    nnoremap <leader>gL :GlistDiff<space>
endif

if exists(':GV')
    nnoremap <leader>gv :GV -n 5000<cr>
    nnoremap <leader>gV :GV -n 5000<space>
endif
