if exists(':CNearest')
    nnoremap <leader>l<space> <cmd>LNearest!<cr>
    nnoremap <leader>q<space> <cmd>CNearest!<cr>
endif
