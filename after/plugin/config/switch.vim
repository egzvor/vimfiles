if !exists(':Switch')
    finish
endif

nnoremap <leader>S <cmd>Switch<cr>
