if !exists(':OstrogaJump')
    finish
endif

nnoremap <leader>' <cmd>OstrogaJump<cr>
