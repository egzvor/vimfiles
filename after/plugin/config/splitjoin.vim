if exists(':SplitjoinJoin')
    finish
endif

nnoremap gJ <cmd>SplitjoinJoin<cr>
nnoremap gS <cmd>SplitjoinSplit<cr>
