if exists('g:loaded_git_gutter_config')
    finish
endif

let g:loaded_git_gutter_config = 1

if exists(':GitGutter')
    let g:gitgutter_max_signs = 1000
    let g:gitgutter_map_keys = 0

    nnoremap _g <cmd>GitGutterToggle<cr>
    nmap [c <Plug>(GitGutterPrevHunk)
    nmap ]c <Plug>(GitGutterNextHunk)

    function! ListRemoteHeads(...)
        return system("git --no-pager branch --remote | cut -d'/' -f 2")
    endfunction

    command! -bar -nargs=1 -complete=custom,ListRemoteHeads GitGutterBase let g:gitgutter_diff_base="origin/" . <q-args> | GitGutter

elseif exists(':SignifyEnable')
    nmap [c <plug>(signify-prev-hunk)
    nmap ]c <plug>(signify-next-hunk)
    nnoremap _g <cmd>SignifyToggle<cr>
endif
