if !exists('*ListArgs')
    finish
endif

nnoremap <leader>a :call ListArgs()<cr>: argu<home>
