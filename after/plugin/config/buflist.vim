if exists('g:loaded_after_buflist')
    finish
endif

let g:loaded_after_buflist = 1

if exists('*ListBuffers')
    nnoremap <leader>b :call ListBuffers()<cr>:buffer<space>
    nnoremap <leader><tab> :call ListBuffers()<cr>:buffer<space>
    nnoremap <leader><space> :call ListBuffers()<cr>:buffer<space>
endif

if exists('g:loaded_scope')
    nnoremap <leader>ff <cmd>ScopeFile<cr>
    nnoremap <leader>b <cmd>ScopeBuffer<cr>
    nnoremap <leader>O <cmd>ScopeOldFiles<cr>
    " TODO: add ScopeTags command?
elseif exists(':Files')
    " fzf.vim
    nnoremap <leader>ff <cmd>Files<cr>
    nnoremap <leader>b <cmd>Buffers<cr>
    nnoremap <leader>T <cmd>Tags<cr>
elseif exists(':PickerEdit')
    nnoremap <leader>ff <cmd>PickerEdit<cr>
    nnoremap <leader>fx <cmd>PickerSplit<cr>
    nnoremap <leader>fv <cmd>PickerVsplit<cr>
    nnoremap <leader>ft <cmd>PickerTabedit<cr>
    nnoremap <leader>b <cmd>PickerBuffer<cr>
    nnoremap <leader>T <cmd>PickerTag<cr>
endif

if exists('*LastModifiedBuffer')
    " TODO: do not attempt buffer 0 if LastModifiedBuffer fails.
    nnoremap <c-^> <cmd>exe ':buffer ' .. LastModifiedBuffer()<cr>
endif

function s:most_relevant(n, ls) abort
    let seen = {}
    let result = []
    let n = a:n

    for [k, l] in a:ls
        for x in l
            if has_key(seen, x.bufnr)
                continue
            endif

            let seen[x.bufnr] = v:null
            call add(result, x)
            let k -= 1
            let n -= 1

            if k == 0
                break
            endif

            if n == 0
                return result
            endif
        endfor
    endfor

    return result
endfunction

function s:list_most_relevant() abort
    let most_relevant = s:most_relevant(8, [
\       [3,   buflist#modified#get()
\           ->buflist#without_current_buffer()
\           ->buflist#without_hidden_buffers()
\           ->buflist#without_terminal_buffers()
\       ],
\       [3,   buflist#freq#most_visited_buffers()
\           ->copy()
\           ->buflist#without_current_buffer()
\           ->buflist#without_hidden_buffers()
\           ->buflist#without_terminal_buffers()
\       ],
\       [8,   buflist#lastused#get()
\           ->copy()
\           ->reverse()
\           ->buflist#without_current_buffer()
\           ->buflist#without_hidden_buffers()
\           ->buflist#without_terminal_buffers()
\       ],
\   ])

    echohl Title
    echo "===Most Relevant==="
    echohl None
    echo most_relevant->buflist#format_buffer_names()->join("\n") .. "\n"
endfunction

" nnoremap <space><space> <cmd>call <sid>list_most_relevant()<cr>:buffer<space>
nnoremap <space><space>
\   <cmd>
\       call BuflistBuffer() \|
\       autocmd CmdlineLeave <buffer> ++once
\           bdelete \| wincmd p
\   <cr>
\   :buffer<space>
