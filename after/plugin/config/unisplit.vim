if exists('*UniversalSplit')
    cnoremap <expr> <c-x> UniversalSplit("horizontal")
    cnoremap <expr> <c-v> UniversalSplit("vertical")
    cnoremap <expr> <c-t> UniversalSplit("tab")
endif
