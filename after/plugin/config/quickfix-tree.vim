if !exists(':QuickfixTree')
    finish
endif

nnoremap <leader>- <cmd>QuickfixTree<cr>
