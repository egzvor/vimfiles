if ! exists('g:loaded_fluffy')
    finish
endif

cmap <space> <plug>(fluffy-space)
cmap <c-/> <plug>(fluffy-toggle)
