if ! exists(':Tagbar')
    finish
endif

nnoremap <leader>tt <cmd>TagbarOpenAutoClose<cr>
nnoremap <leader>ts <cmd>echo tagbar#currenttag('%s', '', 'f', 'scoped-stl')<cr>
