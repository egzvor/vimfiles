if ! exists(':MundoToggle')
    finish
endif

nnoremap _m <cmd>MundoToggle<cr>
