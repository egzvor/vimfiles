if exists(':ToggleQuickfix')
    nnoremap <silent> <leader>qq <cmd>ToggleQuickfix<cr>
    nnoremap <silent> <leader>qo <cmd>ToggleQuickfix<cr>
    nnoremap <silent> <leader>ll <cmd>ToggleLocationFix<cr>
    nnoremap <silent> <leader>lo <cmd>ToggleLocationFix<cr>
endif

" More quickfix mappings:
" after/plugin/config/quickfix-reflector.vim
