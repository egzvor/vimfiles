if !exists(':Zoom')
    finish
endif

nnoremap _z <cmd>Zoom<cr>
