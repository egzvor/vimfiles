if ! exists('*ToggleUnderCursorMatch')
    finish
endif

nnoremap <silent> <leader>i <cmd>call ToggleUnderCursorMatch()<cr>
