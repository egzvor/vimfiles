vim9script
import autoload 'scope/fuzzy.vim'
import autoload 'scope/popup.vim'
import autoload 'scope/util.vim'

var prev_bufsearch = null_string

export def BufIndentSearch(word_under_cursor: bool = false, recall: bool = true)
    if prev_bufsearch == null_string || word_under_cursor
        prev_bufsearch = expand("<cword>")->trim()
    endif
    var lines = []
    var cur_indent = match(getline('.'), '\S')
    var current_line = line('.')
    # TODO: handle last line
    # Add the lines after the current one
    for nr in range(current_line + 1, line('$'))
        var line = getline(nr)
        var non_white_idx = match(line, '\S')
        if non_white_idx == -1 || non_white_idx > cur_indent
            continue
        endif
        if non_white_idx < cur_indent
            break
        endif
        lines->add({text: line, line: line, linenr: nr})
    endfor
    # TODO: handle first line
    # Add the lines before the current one
    for nr in reverse(range(1, current_line - 1))
        var line = getline(nr)
        var non_white_idx = match(line, '\S')
        if non_white_idx == -1 || non_white_idx > cur_indent
            continue
        endif
        if non_white_idx < cur_indent
            break
        endif
        lines->insert({text: $"{line} ({nr})", line: line, linenr: nr})
    endfor
    var menu: popup.FilterMenu
    menu = popup.FilterMenu.new($'Search', lines,
        (res, key) => {
            if !util.Send2Qickfix(key, menu.items_dict, menu.filtered_items[0], 'BufSearch',
                    (v: dict<any>) => {
                        return {bufnr: bufnr(), lnum: v.linenr, text: v.line}
                    })
                exe $":{res.linenr}"
                if menu.prompt != null_string
                    var m = matchfuzzypos([res.line], menu.prompt)
                    if m[1]->len() > 0 && m[1][0]->len() > 0
                        setcharpos('.', [0, res.linenr, m[1][0][0] + 1])
                    endif
                endif
                normal! zz
                prev_bufsearch = menu.prompt
            endif
        },
        (winid, idp) => {
            win_execute(winid, 'syn match ScopeMenuLineNr "(\d\+)$"')
            hi def link ScopeMenuLineNr ScopeMenuSubtle
            hi def link ScopeMenuSubtle Comment
            if recall && prev_bufsearch != null_string
                idp->popup_settext($'{popup.options.promptchar} {prev_bufsearch}')
                idp->clearmatches()
                matchaddpos('ScopeMenuCursor', [[1, 3]], 10, -1, {window: idp})
                matchaddpos('ScopeMenuVirtualText', [[1, 4, 999]], 10, -1, {window: idp})
            endif
        },
        (lst: list<dict<any>>, ctx: string): list<any> => {
            return util.FilterItems(lst, ctx, 'line')
        }, null_function, true)
enddef

command! ScopeFile fuzzy.File('fd -tf --follow')
command! ScopeBuffer fuzzy.Buffer()
command! ScopeOldFiles fuzzy.MRU()
command! ScopeBufSearch fuzzy.BufSearch()
command! ScopeBufIndentSearch BufIndentSearch()
