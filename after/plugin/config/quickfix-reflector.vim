if !exists('#quickfix_reflector')
    finish
endif

" Remove all entries from directory/file under cursor
function GetDir(path)
    let depth = len(split(a:path, '/', 1))
    let current_depth = len(split(trim(a:path[:getpos('.')[2] - 1], '/', 2), '/', 1))
    return escape(fnamemodify(a:path, repeat(':h', depth - current_depth)), '/')
endfunction
nnoremap <leader>l<bs> <cmd>exe 'g/^' . GetDir(bufname(getloclist(0)[line('.') - 1]['bufnr'])) . '/d'<bar>w<cr>
nnoremap <leader>q<bs> <cmd>exe 'g/^' . GetDir(bufname(getqflist()[line('.') - 1]['bufnr'])) . '/d'<bar>w<cr>

augroup AutoQuickfixToggle
    " Remove autocommands from plugin/qftoggle.vim
    autocmd! QuickFixCmdPost
    autocmd QuickFixCmdPost cgetexpr,vimgrep,vimgrepadd copen | doautocmd <nomodeline> quickfix_reflector BufReadPost quickfix
    autocmd QuickFixCmdPost lgetexpr,lvimgrep,lvimgrepadd lopen | doautocmd <nomodeline> quickfix_reflector BufReadPost quickfix
augroup END

" More quickfix mappings:
" after/plugin/config/qftoggle.vim
