if exists(':GrepProject')
    nnoremap <leader>?  :<c-u>GrepProject ''<left>
    nnoremap <leader>/  :<c-u>LGrepProject ''<left>
    nnoremap <leader>lf :<c-u>LGrepFile ''<left>
endif
