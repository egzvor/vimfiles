if ! exists(':ContextToggle')
    finish
endif

let g:context.enabled = 0
let g:context_resize_linewise = 1
nnoremap _c :ContextToggle<cr>
