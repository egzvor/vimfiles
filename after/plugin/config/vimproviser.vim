if ! exists(':VimproviserMap')
    finish
endif

nnoremap <left> <plug>(vimproviser-left)
nnoremap <right> <plug>(vimproviser-right)

nnoremap __ :VimproviserMap<space><c-d>
nnoremap <leader><cr> <cmd>VimproviserLast<cr>
nnoremap ~ <cmd>VimproviserLast<cr>
nnoremap <leader><bs> <cmd>VimproviserMap Characters<cr>

" Make sure trigger mappings won't be overrided.
" Load all the mappings used as triggers now.
runtime after/plugin/config/gitgutter.vim
call VimproviserRegisterTriggers()

augroup MyVimproviserCmds
    au!
    autocmd QuickFixCmdPost cgetexpr VimproviserTrigger Quickfix
    autocmd QuickFixCmdPost lgetexpr VimproviserTrigger LocationList
    autocmd InsertEnter,FocusLost * exe 'VimproviserMap Characters'
augroup END
