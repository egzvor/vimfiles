if ! exists("g:loaded_sandwich")
    finish
endif

call operator#sandwich#set('all', 'all', 'highlight', 0)

nnoremap s <nop>
xnoremap s <nop>
" surround-new
nnoremap sn <Plug>(sandwich-add)
xnoremap sn <Plug>(sandwich-add)
" surround-modify
nnoremap sm <Plug>(sandwich-replace)
xnoremap sm <Plug>(sandwich-replace)
nnoremap smb <Plug>(sandwich-replace-auto)
