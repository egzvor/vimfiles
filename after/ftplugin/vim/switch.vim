let b:switch_custom_definitions = [
\   ['!=', '=='],
\   ['!=?', '==#'],
\   ['!=#', '==?'],
\   ['let &', 'set '],
\]
