setlocal expandtab
setlocal shiftwidth=4

nnoremap <buffer> <localleader>r <cmd>so %<cr>

function! s:mirror_after_path(path) abort
    if match(a:path, '/after/plugin/config/') >= 0
        return substitute(a:path, 'after/plugin/config', 'plugin', '')
    endif

    if match(a:path, '/plugin/') != -1
        return substitute(a:path, 'plugin', 'after/plugin/config', '')
    endif

    throw 'No alternative'
endfunction
nnoremap <buffer> <localleader>a <cmd>execute 'e ' . <sid>mirror_after_path(expand('%:p'))<cr>

augroup VimAdditionalHighlights
    au! * <buffer>
    autocmd WinNew   <buffer> call matchadd('VertSplit', '\v^\s*\zs\\')
    autocmd VimEnter <buffer> call matchadd('VertSplit', '\v^\s*\zs\\')
augroup END
