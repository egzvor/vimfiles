set tabstop=8 shiftwidth=8 noexpandtab
set textwidth=80
setlocal nolist

command! IndentLinuxStyle %!indent -kr -i8

nnoremap <buffer> <localleader>c :!gcc -o%< %<cr>
nnoremap <buffer> <localleader>r :!./%<<cr>
nnoremap <buffer> <localleader>i :IndentLinuxStyle<cr>

highlight link cErrInParen NONE
highlight link cErrinBracket NONE
