if exists(':LspCodeActionSync')
    augroup LspVimGo
    au!
    autocmd BufWritePre <buffer> silent LspCodeActionSync source.organizeImports
    augroup END
endif
