if !exists(':DlvTest')
    finish
endif

nnoremap <buffer> _dd <cmd>DlvToggleBreakpoint<cr>
nnoremap <buffer> _dt <cmd>DlvTest<cr>
