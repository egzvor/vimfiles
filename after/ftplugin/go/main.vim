set noexpandtab shiftwidth=8 tabstop=8 softtabstop=8
setlocal nolist
set formatoptions-=c
set formatoptions+=ro

function! s:switch_to_from_test()
    let path = expand('%:p:r')
    if path =~# '_test$'
        return path[:-6] . '.go'
    endif

    return path . '_test.go'
endfunction

nnoremap <buffer> <localleader>a <cmd>execute 'edit ' . <sid>switch_to_from_test()<cr>

nnoremap <buffer> [[ m'<cmd>call search('\s*func\>\( (.\{-})\)\? \zs\w\+', 'bW')<cr>
nnoremap <buffer> ]] m'<cmd>call search('\s*func\>\( (.\{-})\)\? \zs\w\+', 'W')<cr>

nnoremap <localleader>/f <cmd>lvimgrep /^\s*func/j %<cr>
nnoremap <localleader>/t <cmd>lvimgrep /^\s*type/j %<cr>

if exists(':GoImport')
    nnoremap <buffer> <localleader>i <cmd>exe 'GoImport ' .. expand('<cword>')<cr>
endif

set foldtext=MyFoldText()
function MyFoldText()
  return getline(v:foldstart)
\     ->substitute('\t', repeat(' ', &tabstop), 'g')
\     ->substitute('{$', '{}', '')
endfunction
