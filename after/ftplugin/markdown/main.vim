set expandtab shiftwidth=2 tabstop=2

nnoremap <buffer> <silent> <localleader>p :!pandoc --from markdown --to html --standalone --table-of-contents --incremental % --css=/home/egzvor/configrc/github-pandoc.css --output %:r.html<cr>
nnoremap <buffer> <silent> <localleader>r :call system("firefox " . shellescape(expand("%:r"). ".html"))<cr>
nnoremap <buffer> <silent> <localleader>R :!typora % &<cr><cr>

" Add or remove numbering to selected lines.
" Only unindented lines are numbered.
vnoremap <buffer> <localleader>nd :s/^\S\+ // \| noh<cr>
vnoremap <buffer> <localleader>na :s/^\(\S\)/0. \1/<cr> <esc>gv<c-v>0g<c-a>

command! -buffer -nargs=0 Preview silent exec '!typora "' . expand("%:p") . '"&'
