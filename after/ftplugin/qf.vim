setlocal cursorline
setlocal scrolloff=99
setlocal winfixbuf

nnoremap <buffer> q <cmd>cclose \| lclose<cr>
nnoremap <buffer> <localleader>w :w ~/.vim/lists/

if exists('g:loaded_quickpeek')
    nnoremap <buffer> p <cmd>QuickpeekToggle<cr>
else
    nnoremap <buffer> p <cr>
endif
