setlocal linebreak
nnoremap <buffer> j gj
nnoremap <buffer> k gk
nnoremap <buffer> gj j
nnoremap <buffer> gk k
nnoremap <buffer> 0 g0
nnoremap <buffer> g0 0
