if exists(':Files')
    nnoremap <buffer> <leader>f :Files %<cr>
else
    nnoremap <buffer> <leader>f :edit %:p:h/**/
endif

if exists('g:loaded_relgrep')
    nnoremap <buffer> <leader>? :GrepDir ''<left>
    nnoremap <buffer> <leader>/ :LGrepDir ''<left>
endif

nnoremap <buffer> za :g@\v/\.[^\/]+/?$@d<cr>

nmap <buffer> q <plug>(dirvish_quit)
nnoremap <buffer> <localleader>t :tcd %<cr>

nnoremap <silent><buffer> t <cmd>call dirvish#open('tabedit', 0)<CR>
xnoremap <silent><buffer> t <cmd>call dirvish#open('tabedit', 0)<CR>
