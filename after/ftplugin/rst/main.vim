set expandtab shiftwidth=2 tabstop=2
set textwidth=80

nnoremap <buffer> <localleader>p :exe '!rst2html %:S ' . shellescape(expand("%:r") . '.html')<cr>
nnoremap <buffer> <localleader>r :exe '!firefox ' . shellescape(expand("%:r") . '.html')<cr>

inoremap <buffer> `` ````<left><left>
inoremap <buffer> `<space> <right><right>

" Add or remove numbering to selected lines.
" Only unindented lines are numbered.
vnoremap <buffer> <localleader>nd :s/^\S\+ // \| noh<cr>
vnoremap <buffer> <localleader>na :s/^\(\S\)/0. \1/<cr> <esc>gv<c-v>0g<c-a>

nnoremap <buffer> <localleader>h1 yypVr=<esc>
nnoremap <buffer> <localleader>h2 yypVr-<esc>
nnoremap <buffer> <localleader>h3 yypVr~<esc>
