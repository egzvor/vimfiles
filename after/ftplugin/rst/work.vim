if expand('%:p') !~ $HOME .. '/Notes/Lamoda/'
    finish
endif

set iskeyword+=-,:

function s:five_minute_time() abort
    return strftime('%H:%M', localtime() / 300 * 300)
endfunction

nnoremap <buffer> <localleader>t <cmd>call append(line('.'), ['', $"- {<sid>five_minute_time()} - ..."])<cr>jj^wi

inoremap <buffer> <c-r>! <c-r>=system($HOME .. '/scripts/work_duration.py', getline('.'))<cr>
xnoremap <buffer> <localleader>t :<c-u>exe "'<,'>w !" .. $HOME .. '/scripts/work_duration_sum.py'<cr>
nnoremap <buffer> <localleader>: q:<cmd>call search('^!tlog\>', 'b')<cr>
nnoremap <buffer> <localleader>. ciW...<esc>

xnoremap <buffer> ij <esc>?^\d\d\.\d\d\.\d\d<cr>jjv/^[^$-]<cr>kkV

nnoremap <buffer> [m ?^\d\d\.\d\d\.\d\d<cr>
nnoremap <buffer> ]m /^\d\d\.\d\d\.\d\d<cr>

augroup rstWork
    au!
    au VimLeave call notes#mksession("Session.vim")
augroup END
