let s:PATTERN_SENTINEL = 'HELP_TERM_PATTERN_SENTINEL'

function! s:not_relevant()
    let syntax_group = synIDattr(synID(line('.'), col('.'), 0), 'name')
    return syntax_group ==# 'helpExample' || syntax_group ==# ''
endfunction

function! s:search_term(forward) abort
    " quotation mark for options
    " pipe for help tags
    " backquote for Vim commands
    let surrounds = '[' . "'" . '|' . '`' . ']'
    let pat = surrounds . '\zs\k\+\ze' .  surrounds
    call search(pat, a:forward ? '': 'b', 0, 0, function('s:not_relevant'))
    call setreg('/', s:PATTERN_SENTINEL)
endfunction

function! s:next_term(forward) abort
    if getreg('/') ==# s:PATTERN_SENTINEL
        call s:search_term(a:forward)
        return
    endif

    if a:forward
        normal! n
    else
        normal! N
    endif
endfunction

nnoremap <silent> <buffer> <localleader>f <cmd>call <sid>search_term(1)<cr>
nnoremap <buffer> n <cmd>call <sid>next_term(1)<cr>
nnoremap <buffer> N <cmd>call <sid>next_term(0)<cr>

nnoremap <buffer> q :q<cr>
nnoremap <buffer> <cr> <c-]>

nnoremap <buffer> <leader>? :<c-u>helpgrep<space>
nnoremap <buffer> <leader>/ :<c-u>lhelpgrep<space>
