setlocal spell

if len(getline(1)) == 0
    call cursor(1, 1)
    startinsert
endif
