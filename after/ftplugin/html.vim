setlocal expandtab shiftwidth=4 tabstop=4

nnoremap <silent> <buffer> <localleader>r <cmd>!firefox %<cr>
