if exists('b:loaded_after_python')
    finish
endif

let b:loaded_after_python = 1

set foldmethod=indent
set foldnestmax=2
set nofoldenable
set conceallevel=2

let b:python_highlight_all = 1

function! ListVirtualenvs(...)
    return system('find -L $HOME/.virtualenvs -mindepth 1 -maxdepth 1 -type d')
endfunction

command! -nargs=1 -buffer -bar Pycodestyle cexpr system('git diff-index <args> --name-only --diff-filter=d | grep "\.pyi\?$" | xargs python -m pycodestyle')

" Run code in the file or on the selected lines
xnoremap <buffer> <silent> <localleader>r <cmd>'<,'>w !python<cr>
nnoremap <buffer> <silent> <localleader>r <cmd>w !python<cr>

nnoremap <buffer> <localleader>P m`O__import__("pdb").set_trace()<esc>``
nnoremap <buffer> <localleader>p m`o__import__("pdb").set_trace()<esc>``

nnoremap <localleader>I <cmd>%!isort --profile black - 2>/dev/null<cr>

if exists(':Black')
    nnoremap <buffer> <localleader>b<localleader>b <cmd>Black<cr>
    nmap <buffer> <localleader>b <Plug>(Blackify)
    nmap <buffer> <localleader>bb <Plug>(BlackifyLine)
    xnoremap <buffer> <localleader>b :<c-u>'<,'>Black<cr>
endif
