if ! exists(':ImportName')
    finish
endif

nnoremap <buffer> <localleader>i <cmd>ImportName<cr>
