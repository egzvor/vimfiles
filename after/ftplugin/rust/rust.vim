if ! exists(':RustRun')
    finish
endif

nnoremap <buffer> <localleader>r <cmd>RustRun<cr>
nnoremap <buffer> <localleader>t <cmd>Ctest<cr>
