if exists(':G')
    nnoremap <buffer> <localleader>e <cmd>G show <cword><cr>
endif
