if exists("did_load_filetypes")
  finish
endif

augroup filetypedetect
  au! BufRead,BufNewFile *.yaml.gotmpl setfiletype yaml
augroup END
