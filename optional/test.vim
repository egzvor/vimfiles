if ! exists(':TestFile')
    finish
endif

nnoremap <buffer> <localleader>tf :TestFile<cr>
nnoremap <buffer> <localleader>tF :TestFile<space>
nnoremap <buffer> <localleader>tl :TestLast<cr>
nnoremap <buffer> <localleader>tL :TestLast<space>
nnoremap <buffer> <localleader>tt :TestNearest<cr>
nnoremap <buffer> <localleader>tT :TestNearest<space>
nnoremap <buffer> <localleader>tv :TestVisit<cr>
nnoremap <buffer> <localleader>tV :TestVisit<space>
