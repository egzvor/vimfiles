if exists('g:loaded_after_ale')
    finish
endif

if ! exists(':ALEEnable')
    finish
endif

let g:loaded_after_ale = 1

autocmd! ALEEvents CursorMoved

nnoremap _a <cmd>ALELint<cr>
nnoremap _n <cmd>ALEPopulateLocList<bar>LNearest!<cr>

if exists('*VimproviserRegisterMultiple')
    call VimproviserRegisterMultiple('AleErrors', '_n', '_p')
endif
