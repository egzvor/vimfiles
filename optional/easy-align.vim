if !exists('g:loaded_easy_align_plugin') || exists('g:loaded_easy_align_optional')
    finish
endif

let g:loaded_easy_align_optional = 1

" Start interactive EasyAlign in visual mode (e.g. vip<Enter>)
vmap <Enter> <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
