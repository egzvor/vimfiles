nnoremap gd md<cmd>call tagstack#push()<cr><cmd>YcmCompleter GoToDefinition<cr>
" TODO: Pop the entry if the GoTo command was unsuccessful.
nnoremap gD md<cmd>call tagstack#push() <bar> call tagstack#set_matchnr()<cr><cmd>YcmCompleter GoToImplementation<cr>
nnoremap _t <cmd>call tagstack#jump_to_current()<cr><cmd>YcmCompleter GoToDefinition<cr>
" TODO: figure out matchnr.
nnoremap _lt <cmd>call tagstack#push() <bar> YcmCompleter GoToType <bar> call tagstack#set_tag()<cr>
nnoremap _ln :<c-u>YcmCompleter RefactorRename<space>
nnoremap <c-p> <plug>(YCMHover)
if exists(':VimproviserTrigger')
    nnoremap gr <cmd>exe 'YcmCompleter GoToReferences'<bar>VimproviserTrigger Quickfix<cr>
else
    nnoremap gr <cmd>YcmCompleter GoToReferences<cr>
endif

nnoremap <silent> _lh <Plug>(YCMToggleInlayHints)
nnoremap _ll <Plug>(YCMFindSymbolInWorkspace)
nnoremap _ld <Plug>(YCMFindSymbolInDocument)
