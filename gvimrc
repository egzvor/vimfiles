set guioptions-=T
set guioptions-=m
set guioptions-=r
set guioptions-=L
set guioptions-=e

if has("gui_win32")
    set guifont=Hack:h12:cANSI:qDRAFT
elseif has("gui_gtk")
    " set guifont=Hack\ 13
    set guifont=FiraMono\ 13
endif

colorscheme plan9
